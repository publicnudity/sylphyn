__________________________
# SHORT INSTALLATION GUIDE

1. Rename `config.py.default` to `config.py`
2. Add your **APIKEY** and **TOKEN** to `config.py`
3. Run `python3 -m pip install -U discord.py`
__________________________

# LONG INSTALLATION GUIDE (*FOR BEGINNERS*)

- Rename `config.py.default` to `config.py`
- Add your **APIKEY** and **TOKEN** to `config.py` (instructions in `config.py`)
- Run in **cmd.exe**: `python3 -m pip install -U discord.py`             
	- If (3) worked then setup is done, **you can run the bot now !!!**
	- Otherwise continue with (4) (5) (6) to manually find **pip.exe**.

- Go to installation folder of python to find **pip.exe**

> *example:* `C:\Users\Kappa\AppData\Local\Programs\Python\Python36-32\Scripts`

> *example:* `C:\Users\Program Files\Python\Python36-32\Scripts`

> *example:* `C:\Users\Program Files (x86)\Python\Python36-32\Scripts`

> *example:* `C:\Python\Python36-32\Scripts`

- `shift+rightclick` explorers empty space, `Mouse Menu->Open Command Window Here`

    `Open Command Window here` will open **cmd.exe** in folder `...\Python...\Scripts` so that you can run **pip.exe** directly.

- Run in **cmd.exe**: `pip install -U discord.py`

__________________________
