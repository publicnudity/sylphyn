import urllib.request, json
import math

from config import *
from misc import *

def getInventory(discord_id, numpage):
    with urllib.request.urlopen(REMOTE + "/discord/getInventory/?key=" + APIKEY + "&discord=" + str(discord_id) + "&page=" + str(numpage)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            return data

def getStall(discord_id, numpage):
    with urllib.request.urlopen(REMOTE + "/discord/getStall/?key=" + APIKEY + "&discord=" + str(discord_id) + "&page=" + str(numpage)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            return data

def getInventoryTotal(discord_id):
    with urllib.request.urlopen(REMOTE + "/discord/getInventoryTotal/?key=" + APIKEY + "&discord=" + str(discord_id)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "total" in data:
                return data["total"]
    return None

def findTrainer(discord_id):
    with urllib.request.urlopen(REMOTE + "/discord/findTrainer/?key=" + APIKEY + "&discord=" + str(discord_id)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "exists" in data:
                return data["exists"]
    return False

def newTrainer(discord_id):
    with urllib.request.urlopen(REMOTE +"/discord/newTrainer/?key=" + APIKEY + "&discord=" + str(discord_id)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "completed" in data:
                return data["completed"]
    return False

def capturePokemon(discord_id):
    with urllib.request.urlopen(REMOTE +"/discord/capturePokemon/?key=" + APIKEY + "&discord=" + str(discord_id)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "complete" in data:
                return data
    return None

def getTrainer(discord_id):
    with urllib.request.urlopen(REMOTE +"/discord/getTrainer/?key=" + APIKEY + "&discord=" + str(discord_id)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "exists" in data:
                if data["exists"] == True:
                    return data["trainer"]
    return None

def getNextEnergyReset():
    with urllib.request.urlopen(REMOTE +"/nextEnergyReset/?key=" + APIKEY) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "remaining" in data:
                return data["remaining"]
    return "N/A"

'''
    s is a string of format

        name+XX            (meaning +XX enchant)
        or
        name               (meaning +0  enchant)
'''
def disenchantPokemon(discord_id, s):
    Q = s.split("+")
    E = 0
    N = Q[0]
    if len(Q) == 2:
        if RepresentsInt(Q[1]):
            E = int(Q[1])

    with urllib.request.urlopen("%s/discord/disenchantPokemon/?key=%s&discord=%s&name=%s&enchant=%d" %(REMOTE, APIKEY, str(discord_id), N, E)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if "complete" in data:
                return data["complete"]
    return False


'''
    /discord/enchantPokemon/
    API returns
        completed: true   - ON SUCCESS
            reason: 0     - - - - - - -
        completed: false  - ON FAILURE
            reason: 2     - NO SUCH POKEMON
            reason: 3     - NOT ENOUGH CANDY
            reason: 4     - ENCHANT FAILED (bad luck)
            reason: 9     - INVALID ENCHANT VALUE
        candy:            - REMAINING CANDY
        newenchant:       - NEW ENCHANT ON SUCCESS
'''
def enchantPokemon(discord_id, s):
    Q = s.split("+")
    E = 0
    N = Q[0]
    if len(Q) == 2:
        if RepresentsInt(Q[1]):
            E = int(Q[1])

    with urllib.request.urlopen("%s/discord/enchantPokemon/?key=%s&discord=%s&name=%s&enchant=%d" % (REMOTE, APIKEY, str(discord_id), N, E)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            print(data)
            if ("complete" in data):
                return (data["complete"], data["reason"], data["candy"], data["newenchant"])
    return (False, -1, -1, -1)

	
def newStallEntry(discord_id, s, candy):
    Q = s.split("+")
    E = 0
    N = Q[0]
    C = 0
    if len(Q) == 2:
        if RepresentsInt(Q[1]):
            E = int(Q[1])
    if RepresentsInt(candy):
        C = int(candy)
    with urllib.request.urlopen("%s/discord/newStallEntry/?key=%s&discord=%s&name=%s&enchant=%d&candy=%d" % (REMOTE, APIKEY, str(discord_id), N, E, C)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            print(data)
            if ("complete" in data):
                return data["complete"]
    return False

def buyStallPokemon(discord_id, code):
    with urllib.request.urlopen("%s/discord/buyStallPokemon/?key=%s&discord=%s&code=%s" % (REMOTE, APIKEY, str(discord_id), str(code))) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            print(data)
            if ("complete" in data):
                return data["complete"]
    return False

def getPowerLadder():
    with urllib.request.urlopen("%s/discord/getPowerLadder/?key=%s" % (REMOTE, APIKEY)) as url:
        res = url.read()
        if res != b'':
            data = json.loads(res.decode())
            if ("ladder" in data):
                return (True, data["ladder"], data["total"])
    return (False, )

def printInventoryPage(discord_id, numpage):
    trainer = getTrainer(discord_id)
    
    inventory = getInventory(discord_id, numpage)
    maxpages  = math.ceil(getInventoryTotal(discord_id) / 10)
    candy     = trainer["candy"]
    
    msg = 'Candy ' + u"\U0001F36C" + 'x' + str(candy) + '\r\n\r\n'
    for x in inventory["pokemon"]:
        msg += "#%03d" % (x["id"],)
        msg += " "
        if x["shiny"] == 1:
            msg += "Shiny_"
        msg += x["name"].title()
        msg += " +"
        msg += str(x["enchant"])
        msg += "\r\n"
    msg += "\r\n"
    msg += "- Page ["+str(numpage)+"/" + str(maxpages) + "]"
    msg += "\r\n"
    return msg

def printStallPage(discord_id, numpage):
    trainer = getTrainer(discord_id)
    
    stall = getStall(discord_id, numpage)
    maxpages  = math.ceil(stall["total"] / 10)

    msg = ""
    for x in stall["entries"]:
        msg += "x"
        msg += "%03d" % (x["candy"],)
        msg += " "
        msg += u"\U0001F36C"
        msg += " Candy - #"
        msg += str(x["tradecode"])
        msg += " "
        if x["shiny"] == 1:
            msg += "Shiny_"
        msg += x["name"].title()
        msg += " +"
        msg += str(x["enchant"])
        msg += "\r\n"
    msg += "\r\n"
    msg += "- Page ["+str(numpage)+"/" + str(maxpages) + "]"
    msg += "\r\n"
    return msg


def printPowerLadder():
    ladder = getPowerLadder()
    msg = "Error"
    if ladder[0] == True:
        msg = "\r\n`Rank Power ID#` Discord\r\n"
        for x in ladder[1]:
            msg += "`"
            msg += "%03d" % (x["rank"],)
            msg += "  "
            msg += "%05d" % (x["points"],)
            msg += " "
            msg += "%03d" % (x["id"],)
            msg += "` <@"
            msg += str(x["discord"])
            msg += ">\r\n"
    return msg

