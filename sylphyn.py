#!/bin/python3
import math

# discord
import discord
import asyncio
from collections import deque

# sylphyn
from config import *
from api import *
from misc import *
from random import randint, choice

client = discord.Client()

@client.event
async def on_ready():
    '''
        Called when Sylphyn starts
    '''
    print('<Logged in as>')
    print(client.user.name + ', ' + client.user.id)


@client.event
async def on_reaction_add(reaction, user):
    '''
        Called everytime a Reaction (Emoji) is added to a message
        Use reaction.message to access Message
        Use user to access Member that posted the emoji
        Use reaction.message.author to access Member that posted the message
    '''
    if reaction.message.server == None:
        return #FUCK YOU NO PMs
    if not reaction.message.server.id in WHITELISTED_SERVER_IDS:
        return #FUCK YOU RANDOM SERVERS
    if user.id == client.user.id:
        return # <---------------

    global INVENTORY_MESSAGE_LIST
      
    if reaction.emoji == u'\U0001f1fd': # delete
        try:
            async with INVENTORY_LOCK:
                y = None
                for i,x in enumerate(INVENTORY_MESSAGE_LIST):
                    if x[0].id == reaction.message.id:
                        await client.delete_message(reaction.message)
                        y = x
                        break
                if y != None:
                    INVENTORY_MESSAGE_LIST.remove(y)
                print("deleted 1 message ...")
        except Exception as e:
            print("failed to delete 1 message ...")
            print(e)

@client.event
async def on_message(message):
    '''
        Called everytime someone posts a Message in one of the servers
        our bot joined.
        Called when someone sends our bot a PM as well (message.server == None) for PMs
    '''
    if message.server == None:
        return #FUCK YOU NO PMs
    
    if not message.server.id in WHITELISTED_SERVER_IDS:
        return #FUCK YOU RANDOM SERVERS
    global LAST_MESSAGE_TIMESTAMP
    if getTimeNow() - LAST_MESSAGE_TIMESTAMP < 2:
        return #FUCK YOU NO SPAM

    if message.content.startswith('p.shutdown'):
        try:
            x = getPermissions(message.author.id)
            if x != None:
                if x["rank"] >= 50:
                    await client.send_message(message.channel, '*>>> Shutting down immediately*')
                    await client.logout()
                    await client.close()
                    return
                else:
                    print("Insufficient permissions for `p.shutdown` for <%s> has %d" % (message.author.id, x["rank"]))
            else:
                print("No permissions for `p.shutdown` for <%s>" % message.author.id)
        except Exception as e:
            print(e)

    if message.content.startswith('p.nickname'):
        try:
            x = getPermissions(message.author.id)
            if x != None:
                if x["rank"] >= 100:
                    await client.change_nickname(message.server.me, "실핀愛 Supapapa~")
                    #await client.change_nickname(message.server.me, "PICKLE RICK SUCK A DICK")
                else:
                    print("Insufficient permissions for `p.nickname` for <%s> has %d" % (message.author.id, x["rank"]))
            else:
                print("No permissions for `p.nickname` for <%s>" % message.author.id)
                await client.send_message(message.channel, "Sorry " + message.author.mention + " but my pussy is only for <@145939825864736768>") # B O T S E X    >   B O A T S E X
        except Exception as e:
            print(e)
    
    if message.content.startswith('p.debug'):
        try:
            a = [message.author.id, message.channel.id, message.server.id]
            s = ''.join(a)
            tmp = await client.send_message(message.channel, '<' + message.server.id + '>')
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.testid'):
        try:
            tmp = await client.send_message(message.channel, 'Your ID = ' + str(message.author.id))
            #await client.edit_message(tmp, 'new edited message')
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.candy'):
        try:
            parameter = message.content[len('p.candy'):].strip()
            parameter = parameter.replace('\r\n', ' ')
            parameter = parameter.replace('\n', ' ')
            arr = parameter.split()
            for ii,xx in enumerate(arr):
                if disenchantPokemon(message.author.id, xx): 
                    await client.send_message(message.channel, '[' + str(ii+1) + '/' + str(len(arr)) + '] Hey, ' + message.author.mention + ' I will trade your ' + xx + ' for 1 candy! ... `TRADE COMPLETE`')
                else:
                    await client.send_message(message.channel, '[' + str(ii+1) + '/' + str(len(arr)) + '] Hmmm, ' + message.author.mention + ' looks like you don\'t have a ' + xx + '!')
                await asyncio.sleep(1)
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.enchant') or message.content.startswith('p.plus'):
        try:
            parameter = ""
            if message.content.startswith('p.enchant'):
                parameter = message.content[len('p.enchant'):].strip()
            elif message.content.startswith('p.plus'):
                parameter = message.content[len('p.plus'):].strip()
            if len(parameter) > 0:
                R = enchantPokemon(message.author.id, parameter)
                print(R)
                if R[0] == True:
                    await client.send_message(message.channel, 'Enchanting ' + message.author.mention + '*\'s*  **' + parameter + '** `ENCHANTING COMPLETE`\r\n\r\n'\
                                                                + 'Your **' + parameter + '** is now **+' + str(R[3]) + '**!\r\n'\
                                                                + u"\U0001F36C" + ' x' + str(R[2]) + ' Candy remaining')
                else:
                    if R[1] == 4:
                        await client.send_message(message.channel, 'Enchanting ' + message.author.mention + '*\'s*  **' + parameter + '** `ENCHANTING FAILED`\r\n\r\n'\
                                                                            + u"\U0001F36C" + ' x' + str(R[2]) + ' Candy remaining')
                    elif R[1] == 3:
                        await client.send_message(message.channel, 'Hmmm, ' + message.author.mention + ' looks like you don\'t have enough ' + u"\U0001F36C" + ' Candy!')
                    elif R[1] == 2:
                        await client.send_message(message.channel, 'Hmmm, ' + message.author.mention + ' looks like you don\'t have a ' + parameter + '!')
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.sell'):
        try:
            parameter = message.content[len('p.sell'):].strip()
            parameter = parameter.split()
            name = parameter[0]
            candy = parameter[1]
            if newStallEntry(message.author.id, name, candy): 
                    await client.send_message(message.channel, ' Hello, ' + message.author.mention + ' Your ' + name + ' has been listed for x' + candy + u"\U0001F36C" + ' Candy')
            else:
                    await client.send_message(message.channel, 'Hmmm, ' + message.author.mention + ' looks like you don\'t have a ' + name + '!')
            await asyncio.sleep(1)
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.pokemon'):
        try:
            if findTrainer(message.author.id) == False:
                newTrainer(message.author.id)
            
            result = capturePokemon(message.author.id)
            trainer = getTrainer(message.author.id)
            
            if result["complete"] == True:
                if result["pokemon"]["shiny"] == False:
                    await client.send_file(message.channel, "animated/" + str(result["pokemon"]["id"]) + ".gif")
                    await client.send_message(message.channel, message.author.mention + ' caught ' + decideaoran(result["pokemon"]["name"]) +\
                                          ' **' + result["pokemon"]["name"].title() + '!** Energy **['+str(trainer["energy"])+'/'+str(trainer["maxenergy"])+\
                                          ']** ... Added in his *Pokedex* at **#' + str(result["pokemon"]["id"]) + '**.')
                else:
                    await client.send_file(message.channel, "animated/shiny/" + str(result["pokemon"]["id"]) + ".gif")
                    await client.send_message(message.channel, message.author.mention + ' caught a ' +\
                                          ' **Shiny ' + result["pokemon"]["name"].title() + '!** Energy **['+str(trainer["energy"])+'/'+str(trainer["maxenergy"])+\
                                          ']** ... Added in his *Pokedex* at **#' + str(result["pokemon"]["id"]) + '**.')
                
            else:
                await client.send_message(message.channel, message.author.mention + ", You don't have enough energy!")
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.energy'):   
        try:
            if findTrainer(message.author.id) == False:
                newTrainer(message.author.id)
            
            target = getNextEnergyReset()
            trainer = getTrainer(message.author.id) 
            await client.send_message(message.channel,  message.author.mention + ' You have **['+str(trainer["energy"])+'/'+str(trainer["maxenergy"])+']** Energy ... Next reset in ' + target)
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()
    
    if message.content.startswith('p.version'):
        try:
            msg = '\r\n```' + '실핀愛 v' + VERSION + '\r\n'
            msg += '\r\n'
            msg += '- Discord  @' + message.server.me.name + '#' + message.server.me.discriminator + ' aka ' + message.server.me.display_name + '\r\n'
            msg += '- Email    win@lose.gr\r\n'
            msg += '- Steam    http://steamcommunity.com/id/ahricarri/\r\n'
            msg += '- Dotabuff https://www.dotabuff.com/players/223507899\r\n'
            msg += '\r\n'
            msg += 'Whitelisted servers:\r\n'
            msg += '[PLEB NATION, Public Nudity]'
            msg += '\r\n'
            msg += 'From Public Nudity with <3'
            msg += '```'
            await client.send_message(message.channel, msg)
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.pc'):
        return # p.pc disabled temporarily
        try:
            if findTrainer(message.author.id) == None:
                await client.send_message(message.channel, 'Opening *Someone\'s PC* ... ' + message.author.mention + '\r\n```\r\nNo pokemon caught yet!\r\n```')
            else:
                msg = printInventoryPage(message.author.id, 1)
                cmsg = await client.send_message(message.channel, 'Opening *Someone\'s PC* ... ' + message.author.mention + '\r\n```\r\n' + msg + '\r\n```')
                await asyncio.sleep(0.5)
                await client.add_reaction(cmsg, u'\U0001f1fd')
                async with INVENTORY_LOCK:
                    INVENTORY_MESSAGE_LIST.appendleft([cmsg, message.author, 1])

        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.pn'):
        try:
            parameter = message.content[len('p.pn '):].strip()
            if findTrainer(message.author.id) != False:
                maxpages = getInventoryTotal(message.author.id)
                
                y = 1
                n = None
                if RepresentsInt(parameter):
                    n = int(parameter)
                    if n >= 1:
                        y = n
                
                q = None
                async with INVENTORY_LOCK:
                    for i,x in enumerate(INVENTORY_MESSAGE_LIST):
                        if x[1].id == message.author.id:
                            if n == None:
                                if x[2]+1 <= maxpages:
                                    y = x[2] + 1
                                else:
                                    y = x[2]
                            await client.delete_message(x[0])
                            q = x
                            break
                    if q != None:
                        INVENTORY_MESSAGE_LIST.remove(q)

                y = clamp(y, 1, maxpages)
                        
                msg = printInventoryPage(message.author.id, y)
                cmsg = await client.send_message(message.channel, 'Opening *Someone\'s PC* ... ' + message.author.mention + '\r\n```\r\n' + msg + '\r\n```')
                await asyncio.sleep(0.5)
                await client.add_reaction(cmsg, u'\U0001f1fd')
                async with INVENTORY_LOCK:
                    INVENTORY_MESSAGE_LIST.appendleft([cmsg, message.author, y])

                await client.delete_message(message)

        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()


    if message.content.startswith('p.pp'):
        try:
            parameter = message.content[len('p.pp '):].strip()
            if findTrainer(message.author.id) != False:
                maxpages = getInventoryTotal(message.author.id)

                y = 1
                n = None
                if RepresentsInt(parameter):
                    n = int(parameter)
                    if n >= 1:
                        y = n
                
                q = None
                async with INVENTORY_LOCK:
                    for i,x in enumerate(INVENTORY_MESSAGE_LIST):
                        if x[1].id == message.author.id:
                            if n == None:
                                if x[2]-1 > 0:
                                    y = x[2] - 1
                                else:
                                    y = x[2]
                            q = x
                            await client.delete_message(x[0])
                            break
                    if q != None:
                        INVENTORY_MESSAGE_LIST.remove(q)

                y = clamp(y, 1, maxpages)
                
                msg = printInventoryPage(message.author.id, y)
                cmsg = await client.send_message(message.channel, 'Opening *Someone\'s PC* ... ' + message.author.mention + '\r\n```\r\n' + msg + '\r\n```')
                await asyncio.sleep(0.5)
                await client.add_reaction(cmsg, u'\U0001f1fd')
                async with INVENTORY_LOCK:
                    INVENTORY_MESSAGE_LIST.appendleft([cmsg, message.author, y])

                await client.delete_message(message)

        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.stall'):
        try:
            parameter = message.content[len('p.stall '):].strip()
            PAGE = 1
            if RepresentsInt(parameter):
                PAGE = int(parameter)

            msg = printStallPage(message.author.id, PAGE)
            cmsg = await client.send_message(message.channel, '`SALE` ' + message.author.mention + '\r\n```\r\n' + msg + '\r\n```')
            
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.buy'):
        try:
            parameter = message.content[len('p.buy '):].strip().strip("#")
            result = buyStallPokemon(message.author.id, parameter)
            if result == True:
                cmsg = await client.send_message(message.channel, 'Successfully bought, ' + message.author.mention + ' trade order #' + parameter)
            else:
                cmsg = await client.send_message(message.channel, 'Hmmm, something went wrong while buying ' + message.author.mention + ' trade order #' + parameter)
            
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()


    if message.content.startswith('p.power'):
        try:
            msg = printPowerLadder()
            cmsg = await client.send_message(message.channel, '`WORLDWIDE POWER LADDER`\r\n\r\n' + msg + '\r\n')
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()


    if message.content.startswith('p.help'):
        try:
            await client.send_message(message.channel, 'Showing *INSTRUCTIONS* ... ' + message.author.mention + '\r\n```\r\n' + HELP + '\r\n```')
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.changelog'):
        try:
            await client.send_message(message.channel, 'Showing *CHANGE LOG* ... ' + message.author.mention + '\r\n```\r\n' + CHANGELOG + '\r\n```')
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.cccp'):
        try:
            if message.author.id == "145939825864736768" or message.author.id == "163539231573016576":
                await client.send_message(message.channel, "ATTENTION FREE WORKERS " + message.author.mention + ' HAS SUMMONED')
                if PREFER_EMBED:
                    e = discord.Embed(color=0xff0000)
                    e.set_image(url="https://new4.fjcdn.com/pictures/Stalin_986531_522456.jpg")
                    await client.send_message(message.channel, embed=e)
                elif PREFER_UPLOAD:
                    await client.send_file(message.channel, "custom/kke.jpg")
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.kke'):
        try:
            if message.author.id == "145939825864736768" or message.author.id == "163539231573016576":
                if PREFER_EMBED:
                    l = [ ["", "https://i.redd.it/cqkjxaswlc0z.jpg"] ]
                    e = discord.Embed(color=0xff0000)
                    e.set_image(url=choice(l)[1])
                    await client.send_message(message.channel, embed=e)
                elif PREFER_UPLOAD:
                    pass
                    #await client.send_file(message.channel, "custom/kke.jpg")
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('!!porn_gif'):
        try:
            if randint(1, 100) <= 25:
                q = randint(1, 100)
                print("!!porn_gif -> (%d)" % q)
                L = ["custom/angrymom.jpg", "custom/angryold.jpg", "custom/surveillance.jpg", "custom/fbi.jpg", "custom/parents.jpg", "custom/assasin.jpg"]
                R = ["https://beewol.files.wordpress.com/2012/07/regret.png",\
                     "https://i.imgur.com/x43vCRB.gif",\
                     "https://i.imgur.com/LwM8Ahp.gif",\
                     "https://i.imgur.com/ANTLgMi.mp4",\
                     "https://i.imgur.com/VlbZoxy.gif",\
                     "https://i.imgur.com/pTRyCnd.gif",\
                     "https://i.imgur.com/O4G8s7P.gif",\
                     "https://i.imgur.com/gBBe7hB.gif",\
                     "https://i.imgur.com/HAoV37i.gif"]
                if q < 30:
                    await client.send_file(message.channel, choice(L))
                if q >= 30:
                    e = discord.Embed(color=0xff5714)
                    e.set_image(url=choice(R))
                    await client.send_message(message.channel, embed=e)
                    
                LAST_MESSAGE_TIMESTAMP = getTimeNow()
        except Exception as e:
            print(e)

    if message.content.startswith('p.rem'):
        try:
            if PREFER_EMBED:
                e = discord.Embed(color=0xff5714)
                e.set_image(url="https://i.giphy.com/media/pcs8rHmEHpc3K/giphy.gif")
                await client.send_message(message.channel, embed=e)
            elif PREFER_UPLOAD:
                await client.send_file(message.channel, "custom/REM.gif")
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    if message.content.startswith('p.umaru'):
        try:
            l = [["cola.gif", "https://img.fireden.net/a/image/1445/82/1445824595407.gif"], \
                 ["dance.gif", "file:///C:/Users/Kappa/Desktop/SV/custom/umaru/dance.gif"], \
                 ["happy.gif", "https://media.giphy.com/media/v60KQg3MXLwTS/giphy.gif"]]
            if PREFER_EMBED:
                e = discord.Embed(color=0xff5714)
                e.set_image(url=choice(l)[1])
                await client.send_message(message.channel, embed=e)
            elif PREFER_UPLOAD:
                await client.send_file(message.channel, "custom/umaru/" + choice(l)[0])
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

    #https://coolors.co/app/1be7ff-6eeb83-e4ff1a-e8aa14-ff5714
    if message.content.startswith('p.hi'):
        try:
            l = [ ["happy.gif", "https://img.fireden.net/a/image/1445/92/1445923703464.gif"], \
                  ["point.gif", "https://78.media.tumblr.com/dfd3a0e13c05b8fcafaa76c1f2007cab/tumblr_ns43bdCGYe1qg78wpo1_500.gif"], \
                  ["rumble.gif", "https://78.media.tumblr.com/f4cbfb108b10d6c26dbe91867e38b36b/tumblr_ntekvjHgv91s514nio1_500.gif"], \
                  ["yes.gif", "https://thumbs.gfycat.com/AlarmedColossalAmphiuma-size_restricted.gif"], \
                ]
            if PREFER_EMBED:
                e = discord.Embed(color=0x1be7ff)
                e.set_image(url=choice(l)[1])
                await client.send_message(message.channel, embed=e)
            elif PREFER_UPLOAD:
                await client.send_file(message.channel, "custom/sylphyn/" + choice(l)[0])
        except Exception as e:
            print(e)
        LAST_MESSAGE_TIMESTAMP = getTimeNow()

# AS A BOT ACCOUNT (RECOMMENDED)
# 
# !!!! NEVER SHARE YOUR TOKEN !!!!
client.run(TOKEN)

# client.run("myemail@gmail.com", "password")
#
# ^^^ THIS IS CALLED `SELFBOT` AND IT IS (NOT RECOMMENDED) !!!
#     YOUR MAIN ACCOUNT MIGHT BE BANNED IF YOU SPAM DISCORD's API !!!
# -------------------- /DISCORD ---------------------------

