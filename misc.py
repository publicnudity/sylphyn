import time

def clamp(val, min_, max_):
    return min_ if val < min_ else max_ if val > max_ else val

def RepresentsInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False

def decideaoran(string):
    vowel = ["a","e","i","o","u"]
    if string[0].lower() in vowel:
        return "an"
    else:
        return "a"

def getTimeNow():
    return int(time.time())

def getPermissions(discord_id):
    for x in PERMISSIONS:
        if x["id"] == discord_id:
            return x
    return None
